%echo example as an application
%Richard Stewing; 3.5.2014; Dorsten, Germany

%app.erl

-module (ech_app).
-behaviour (application).
-export ([start/2, stop/1]).


start(_Type, _StartArgs) ->
    ech_sup:start_link().
stop(_State) ->
    ok.