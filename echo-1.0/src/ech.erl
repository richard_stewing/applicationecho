%echo example as an application
%Richard Stewing; 3.5.2014; Dorsten, Germany

%genServer.erl

-module (ech).
-behaviour (gen_server).
-export ([start_link/0, stop/0, init/1, handle_cast/2, terminate/2, send_message/1, loop/0, rec/0]).

start_link()-> 
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop()->
	gen_server:cast(?MODULE, stop).


%starts the server with the loop data
init([])->
	Pid=spawn(ech, loop, []),
	Pid2=spawn(ech, rec, []),
	{ok, {Pid, Pid2}}.

%callback functions 

handle_cast(stop, LoopData) ->
  {stop, normal, LoopData};

handle_cast(Mg, {To, From})->
 	To ! {From, Mg},
 	{noreply, {To, From}}.

terminate(_Reason, LoopData) ->
  LoopData.

%client fucntion

send_message(Mg)->
	gen_server:cast(?MODULE, Mg).


%process

loop()->
	receive
		{From, Mg} -> From ! Mg ;
		_Other -> {error, noMatch}
	end,
	loop().




rec()-> 
	receive 
		Mg -> io:format("~w  is the message~n", [Mg])
	end,
	rec().




